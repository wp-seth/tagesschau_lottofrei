// ==UserScript==
// @name     tagesschau_lottofrei
// @description tagesschau ohne lotto
// @include     https://www.tagesschau.de/*
// @version  1
// @grant    none
// ==/UserScript==

function rmv(obj){
	if(obj){
		obj.parentNode.removeChild(obj);
	}
}

function remove_lotto(){
	const sections = document.querySelectorAll('.teasergroup, .section');
	for(let s = 0; s < sections.length; ++s){
		const lotto = sections[s].querySelectorAll('.lottoBox, .lotto');
		if(lotto.length > 0){
			rmv(sections[s]);
			console.log("deleted lotto");
			/* --s; no, because js does not shift */
		}
	}
}

remove_lotto();
