# tagesschau_lottofrei

view tagesschau.de without (annoying) lottery (lotto) numbers

this script tries to remove every "news" about lotto on tagesschau.de..

## prerequisites
- install firefox
- install [greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/)

## installation
- go to [tagesschau_lottofrei.js](tagesschau_lottofrei.js)
- click on the icon for "copy file contents" or just copy the file content
- in firefox: greasemonkey -> new user script -> paste
